package ipinfo

import (
	"fmt"
	"net"
	"regexp"
)

func allowed(iface string) bool {
	// blacklistName
	for _, blacklistName := range []string{
		"lo",
		"docker0",
	} {
		if iface == blacklistName {
			return false
		}
	}
	// whitelist
	for _, el := range []string{
		"external",
	} {
		if iface == el {
			return true
		}
	}
	// whitelist regexp
	for _, r := range []regexp.Regexp{
		*regexp.MustCompile("^en.*"),
		*regexp.MustCompile("^eth.*"),
	} {
		if r.MatchString(iface) {
			return true
		}
	}
	return false
}

func IpInfo() (string, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, inter := range interfaces {
		if !allowed(inter.Name) {
			continue
		}
		addr, err := inter.Addrs()
		if err != nil {
			return "", err
		}
		for _, el := range addr {
			ip, _, err := net.ParseCIDR(el.String())
			if err != nil {
				return "", err
			}
			return ip.String(), nil
		}
	}
	return "", fmt.Errorf("failed to find iface")
}
