package metric

import (
	"bytes"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	"go.uber.org/atomic"
	"go.uber.org/zap"
)

var (
	DefaultLabels = map[string]string{
		"hostname": os.Getenv("HOSTNAME"),
	}
)

func New(name, url string, labels map[string]string, log *zap.Logger) Metric {
	labelName := ""
	labelValue := ""
	i := 2
	for key, value := range DefaultLabels {
		labelName += fmt.Sprintf(",%d:label:%s", i, key)
		labelValue += fmt.Sprintf(",%s", value)
		i += 1
	}
	for key, value := range labels {
		labelName += fmt.Sprintf(",%d:label:%s", i, key)
		labelValue += fmt.Sprintf(",%s", value)
		i += 1
	}
	log.Debug("registered metric", zap.String("name", name), zap.String("labels", labelName+labelValue))
	//goland:noinspection GoDeprecation
	return Metric{
		Name: name,
		netClient: &http.Client{
			Timeout: time.Second * 10,
			Transport: &http.Transport{
				Dial: (&net.Dialer{
					Timeout: 5 * time.Second,
				}).Dial,
				TLSHandshakeTimeout: 5 * time.Second,
			},
		},
		rawURL:     url,
		labelName:  labelName,
		labelValue: labelValue,
		log:        log,
	}
}

type Metric struct {
	atomic.Float64
	Name, rawURL, labelName, labelValue string
	netClient                           *http.Client
	log                                 *zap.Logger
}

func (m Metric) Ping() error {
	pingURL := m.rawURL + "/api/v1/status/active_queries"
	response, err := m.netClient.Get(pingURL)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("invalid status code for %s: %d", pingURL, response.StatusCode)
	}
	return nil
}

func (m Metric) Send() error {
	writeURL := m.rawURL + "/api/v1/import/csv?format=1:metric:" + m.Name + m.labelName
	payload := m.String() + m.labelValue
	req, err := http.NewRequest(http.MethodPost, writeURL, bytes.NewBuffer([]byte(payload)))
	if err != nil {
		return fmt.Errorf("failed to prepare request: %s", err)
	}
	if res, err := m.netClient.Do(req); err != nil {
		return fmt.Errorf("can't do request: %s", err)
	} else {
		if res.StatusCode != http.StatusNoContent {
			return fmt.Errorf("recived invalid status code: %d", res.StatusCode)
		}
	}
	if m.log != nil {
		m.log.Info("sended", zap.String("name", m.Name), zap.Float64("value", m.Load()))
	}
	return nil
}

func (m Metric) FromFunction(f func() (float64, error)) {
	if v, err := f(); err != nil {
		m.Error("failed to gather metric from function", err)
		return
	} else {
		m.Store(v)
		if err := m.Send(); err != nil {
			m.Error("failed to write metric from function", err)
			return
		}
	}
}

func (m Metric) Error(msg string, err error) {
	if m.log != nil {
		m.log.Error(msg, zap.Error(err))
	}
}

func (m Metric) StoreAndSend(val float64) error {
	m.Store(val)
	return m.Send()
}
