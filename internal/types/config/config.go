package config

import (
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/binance"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/kyc"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/rpc"
)

type Config struct {
	VictoriaMetrics string         `json:"victoria-metrics" yaml:"victoria-metrics" default:"http://127.0.0.1:8428" flag:"vm" env:"VM"`
	RPC             rpc.Config     `json:"rpc" yaml:"rpc"`
	Binance         binance.Config `json:"binance" yaml:"binance"`
	KYC             kyc.Config     `json:"kyc" yaml:"kyc"`
}
