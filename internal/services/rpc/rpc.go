package rpc

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/portto/solana-go-sdk/client"
	"github.com/portto/solana-go-sdk/rpc"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/hclient"
	"go.uber.org/zap"
)

func New(cfg Config, vm string, logger *zap.Logger) Client {
	logger.Sugar().Info("created rpc.Client", cfg)
	return Client{
		Config:          cfg,
		HTTPClient:      &hclient.HClient,
		SolanaClient:    client.NewClient(cfg.Endpoint),
		RPCClient:       rpc.NewRpcClient(cfg.Endpoint),
		VictoriaMetrics: vm,
		Logger:          logger,
	}
}

type Client struct {
	HTTPClient      *http.Client
	Config          Config
	VictoriaMetrics string
	Logger          *zap.Logger
	SolanaClient    *client.Client
	RPCClient       rpc.RpcClient
}

func (c Client) getContext() context.Context {
	//goland:noinspection GoVetLostCancel
	ctx, _ := context.WithTimeout(context.TODO(), 5*time.Second)
	return ctx
}

func (c Client) Version() (string, error) {
	r, err := c.SolanaClient.GetVersion(c.getContext())
	if err != nil {
		return "", err
	}
	return r.SolanaCore, nil
}

func (c Client) Balance(base58Addr string) (float64, error) {
	balance, err := c.SolanaClient.GetBalance(c.getContext(), base58Addr)
	if err != nil {
		return -1, err
	}
	return float64(balance), nil
}

func (c Client) GetTokenSupply(mintAddr string) (float64, error) {
	supply, _, err := c.SolanaClient.GetTokenSupply(c.getContext(), mintAddr)
	if err != nil {
		return -1, err
	}
	return float64(supply), nil
}

func (c Client) MinimumLedgerSlot() (float64, error) {
	mlsr, err := c.RPCClient.MinimumLedgerSlot(c.getContext())
	if err != nil {
		return -1, err
	}
	return float64(mlsr.Result), nil
}

type EpochInfo struct {
	rpc.GeneralResponse
	Result struct {
		AbsoluteSlot     float64 `json:"absoluteSlot"`
		BlockHeight      float64 `json:"blockHeight"`
		Epoch            float64 `json:"epoch"`
		SlotIndex        float64 `json:"slotIndex"`
		SlotsInEpoch     float64 `json:"slotsInEpoch"`
		TransactionCount float64 `json:"transactionCount"`
	} `json:"result"`
}

func (c Client) GetEpochInfo() (*EpochInfo, error) {
	var epochInfo EpochInfo
	res, err := c.RPCClient.Call(c.getContext(), "getEpochInfo")
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(res, &epochInfo); err != nil {
		return nil, err
	}
	return &epochInfo, nil
}

// getBlockProduction
type BlockProductionInfo struct {
	rpc.GeneralResponse
	Result struct {
		Context struct {
			Slot float64 `json:"slot"`
		} `json:"context"`
		Value struct {
			ByIdentity interface{} `json:"byIdentity"`
			//ByIdentity map[string][]float64 `json:"byIdentity"`
			//ByIdentity struct {
			//	Eight5IYT5RuzRTDgjyRa3CP8SYhM2J21Fj7NhfJ3Peu1DPr []int `json:"85iYT5RuzRTDgjyRa3cP8SYhM2j21fj7NhfJ3peu1DPr"`
			//} `json:"byIdentity"`
			Range struct {
				FirstSlot float64 `json:"firstSlot"`
				LastSlot  float64 `json:"lastSlot"`
			} `json:"range"`
		} `json:"value"`
	} `json:"result"`
}

func (c Client) GetBlockProduction() (*BlockProductionInfo, error) {
	opts := struct {
		Identity string `json:"identity,omitempty"`
	}{
		Identity: c.Config.Identity,
	}

	var blockProductionInfo BlockProductionInfo
	res, err := c.RPCClient.Call(c.getContext(), "getBlockProduction", opts)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(res, &blockProductionInfo); err != nil {
		return nil, err
	}
	return &blockProductionInfo, nil
}

type VoteAccountsNode struct {
	Commission       float64     `json:"commission"`
	EpochVoteAccount bool        `json:"epochVoteAccount"`
	EpochCredits     [][]float64 `json:"epochCredits"`
	NodePubkey       string      `json:"nodePubkey"`
	LastVote         float64     `json:"lastVote"`
	ActivatedStake   float64     `json:"activatedStake"`
	VotePubkey       string      `json:"votePubkey"`
}

type VoteAccountsInfo struct {
	rpc.GeneralResponse
	Result struct {
		Current    []VoteAccountsNode `json:"current"`
		Delinquent []VoteAccountsNode `json:"delinquent"`
	} `json:"result"`
}

func (c Client) GetVoteAccounts() (*VoteAccountsInfo, error) {
	opts := struct {
		VotePubkey string `json:"votePubkey,omitempty"`
	}{
		VotePubkey: c.Config.VotePublicKey,
	}

	var vai VoteAccountsInfo
	res, err := c.RPCClient.Call(c.getContext(), "getVoteAccounts", opts)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(res, &vai); err != nil {
		return nil, err
	}
	return &vai, nil
}

func (c Client) GetEpochSchedule() (rpc.GetEpochScheduleResponse, error) {
	return c.RPCClient.GetEpochSchedule(c.getContext())
}

type GetIdentityInfo struct {
	rpc.GeneralResponse
	Result struct {
		Identity string `json:"identity"`
	} `json:"result"`
}

func (c Client) GetIdentity() (GetIdentityInfo, error) {
	var (
		vai GetIdentityInfo
	)
	res, err := c.RPCClient.Call(c.getContext(), "getIdentity")
	if err != nil {
		return vai, err
	}
	if err := json.Unmarshal(res, &vai); err != nil {
		return vai, err
	}
	return vai, nil
}
