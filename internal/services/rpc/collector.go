package rpc

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/egeneralov/solana-push-exporter/internal/types/metric"
	"go.uber.org/zap"
)

func (c Client) Collector(ctx context.Context) {
	version, err := c.Version()
	if err != nil {
		c.Logger.Error("failed to read solana version", zap.Error(err))
	}
	metricLogger := c.Logger.With(zap.String("subsystem", "metric"))

	// GetEpochSchedule
	go func() {
		for {
			select {
			default:
				info, err := c.GetEpochSchedule()
				if err == nil {
					if err := metric.New("first_normal_epoch", c.VictoriaMetrics, nil, metricLogger).StoreAndSend(float64(info.Result.FirstNormalEpoch)); err != nil {
						c.Logger.Error("failed to send first_normal_epoch", zap.Error(err))
					}
					if err := metric.New("first_normal_slot", c.VictoriaMetrics, nil, metricLogger).StoreAndSend(float64(info.Result.FirstNormalSlot)); err != nil {
						c.Logger.Error("failed to send first_normal_slot", zap.Error(err))
					}
					if err := metric.New("slots_per_epoch", c.VictoriaMetrics, nil, metricLogger).StoreAndSend(float64(info.Result.SlotsPerEpoch)); err != nil {
						c.Logger.Error("failed to send slots_per_epoch", zap.Error(err))
					}
					if err := metric.New("leader_schedule_slot_offset", c.VictoriaMetrics, nil, metricLogger).StoreAndSend(float64(info.Result.LeaderScheduleSlotOffset)); err != nil {
						c.Logger.Error("failed to send leader_schedule_slot_offset", zap.Error(err))
					}
				}
				time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
			case <-ctx.Done():
				break
			}
		}
	}()

	// GetVoteAccounts
	go func() {
		for {
			select {
			default:
				vai, err := c.GetVoteAccounts()
				if err != nil {
					c.Logger.Error("GetVoteAccounts()", zap.Error(err))
					continue
				}

				delinquentCount := metric.New("delinquent_count", c.VictoriaMetrics, nil, metricLogger)
				delinquentCount.Store(float64(len(vai.Result.Delinquent)))
				if err := delinquentCount.Send(); err != nil {
					c.Logger.Error("failed to send delinquent_count", zap.Error(err))
				}

				currentCount := metric.New("current_count", c.VictoriaMetrics, nil, metricLogger)
				currentCount.Store(float64(len(vai.Result.Current)))
				if err := currentCount.Send(); err != nil {
					c.Logger.Error("failed to send current_count", zap.Error(err))
				}

				processNode := func(node VoteAccountsNode, delinquent bool, l map[string]string) {
					lastVote := metric.New("node_last_vote", c.VictoriaMetrics, l, metricLogger)
					lastVote.Store(node.LastVote)
					if err := lastVote.Send(); err != nil {
						c.Logger.Error("failed to send lastVote", zap.Error(err))
					}
					if err := metric.New("activated_stake", c.VictoriaMetrics, l, metricLogger).StoreAndSend(node.ActivatedStake); err != nil {
						c.Logger.Error("failed to send activated_stake", zap.Error(err))
					}
					commission := metric.New("commission", c.VictoriaMetrics, l, metricLogger)
					commission.Store(node.Commission)
					if err := commission.Send(); err != nil {
						c.Logger.Error("failed to send commission", zap.Error(err))
					}
					isDelinquent := metric.New("delinquent", c.VictoriaMetrics, l, metricLogger)
					if delinquent {
						isDelinquent.Store(1)
					} else {
						isDelinquent.Store(0)
					}
					if err := isDelinquent.Send(); err != nil {
						c.Logger.Error("failed to send isDelinquent", zap.Error(err))
					}

					var EpochCredits float64
					for _, arr := range node.EpochCredits {
						if len(arr) == 3 {
							EpochCredits = arr[1]
						}
					}
					if err := metric.New("epoch_credits", c.VictoriaMetrics, l, metricLogger).StoreAndSend(EpochCredits); err != nil {
						c.Logger.Error("failed to send epoch_credits", zap.Error(err))
					}

				}

				totalActiveStake := float64(0)
				totalDelinquentStake := float64(0)

				var l map[string]string

				for _, node := range vai.Result.Current {
					l = map[string]string{"nodePubkey": node.NodePubkey, "votePubkey": node.VotePubkey}
					processNode(node, false, l)
					totalActiveStake += node.ActivatedStake
				}

				for _, node := range vai.Result.Delinquent {
					l = map[string]string{"nodePubkey": node.NodePubkey, "votePubkey": node.VotePubkey}
					processNode(node, true, l)
					totalActiveStake += node.ActivatedStake
					totalDelinquentStake += node.ActivatedStake
				}

				if err := metric.New("total_active_stake", c.VictoriaMetrics, l, metricLogger).StoreAndSend(totalActiveStake); err != nil {
					c.Logger.Error("failed to send total_active_stake", zap.Error(err))
				}

				if err := metric.New("total_delinquent_stake", c.VictoriaMetrics, l, metricLogger).StoreAndSend(totalDelinquentStake); err != nil {
					c.Logger.Error("failed to send total_delinquent_stake", zap.Error(err))
				}

				pctTotDelinquent := 100 * totalDelinquentStake / totalActiveStake
				if err := metric.New("pct_total_delinquent", c.VictoriaMetrics, l, metricLogger).StoreAndSend(pctTotDelinquent); err != nil {
					c.Logger.Error("failed to send pct_total_delinquent", zap.Error(err))
				}

				time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
			case <-ctx.Done():
				break
			}
		}
	}()

	// getBlockProduction
	go func() {
		labels := map[string]string{"version": version, "identity": c.Config.Identity}
		l := c.Logger.With(zap.String("subsystem", "epoch_info"))
		bpContextSlot := metric.New("block_production_context_slot", c.VictoriaMetrics, labels, metricLogger)
		bpFirstSlot := metric.New("block_production_first_slot", c.VictoriaMetrics, labels, metricLogger)
		bpLastSlot := metric.New("block_production_last_slot", c.VictoriaMetrics, labels, metricLogger)
		for {
			select {
			default:
				info, err := c.GetBlockProduction()
				if err == nil {
					bpContextSlot.Store(info.Result.Context.Slot)
					if err := bpContextSlot.Send(); err != nil {
						l.Error("failed to send block_production_context_slot", zap.Error(err))
					}

					bpFirstSlot.Store(info.Result.Value.Range.FirstSlot)
					if err := bpFirstSlot.Send(); err != nil {
						l.Error("failed to send block_production_first_slot", zap.Error(err))
					}

					bpLastSlot.Store(info.Result.Value.Range.LastSlot)
					if err := bpLastSlot.Send(); err != nil {
						l.Error("failed to send block_production_last_slot", zap.Error(err))
					}
				}

				time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
			case <-ctx.Done():
				break
			}
		}
	}()

	// GetEpochInfo
	go func() {
		labels := map[string]string{"version": version}
		l := c.Logger.With(zap.String("subsystem", "epoch_info"))
		epoch := metric.New("epoch", c.VictoriaMetrics, labels, metricLogger)
		blockHeight := metric.New("block_height", c.VictoriaMetrics, labels, metricLogger)
		absoluteSlot := metric.New("absolute_slot", c.VictoriaMetrics, labels, metricLogger)
		slotIndex := metric.New("slot_index", c.VictoriaMetrics, labels, metricLogger)
		slotsInEpoch := metric.New("slots_in_epoch", c.VictoriaMetrics, labels, metricLogger)
		transactionCount := metric.New("transaction_count", c.VictoriaMetrics, labels, metricLogger)
		for {
			select {
			default:
				info, err := c.GetEpochInfo()
				if err == nil {
					epoch.Store(info.Result.Epoch)
					if err := epoch.Send(); err != nil {
						l.Error("failed to send epoch", zap.Error(err))
					}

					blockHeight.Store(info.Result.BlockHeight)
					if err := blockHeight.Send(); err != nil {
						l.Error("failed to send blockHeight", zap.Error(err))
					}

					absoluteSlot.Store(info.Result.AbsoluteSlot)
					if err := blockHeight.Send(); err != nil {
						l.Error("failed to send absoluteSlot", zap.Error(err))
					}

					slotsInEpoch.Store(info.Result.SlotsInEpoch)
					if err := slotsInEpoch.Send(); err != nil {
						l.Error("failed to send slotsInEpoch", zap.Error(err))
					}

					absoluteSlot.Store(info.Result.AbsoluteSlot)
					if err := absoluteSlot.Send(); err != nil {
						l.Error("failed to send absoluteSlot", zap.Error(err))
					}

					slotIndex.Store(info.Result.SlotIndex)
					if err := slotIndex.Send(); err != nil {
						l.Error("failed to send slotIndex", zap.Error(err))
					}

					transactionCount.Store(info.Result.TransactionCount)
					if err := transactionCount.Send(); err != nil {
						l.Error("failed to send transactionCount", zap.Error(err))
					}
				}

				time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
			case <-ctx.Done():
				break
			}
		}
	}()

	// balances
	for _, addr := range c.Config.Addresses {
		go func(addr string) {
			m := metric.New("balance", c.VictoriaMetrics, map[string]string{"version": version, "address": addr}, metricLogger)
			for {
				select {
				default:
					m.FromFunction(func() (float64, error) {
						f, err := c.Balance(addr)
						if err != nil {
							return -1, fmt.Errorf("failed to get balance for %s: %s", addr, err)
						}
						return f, nil
					})
					time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
				case <-ctx.Done():
					break
				}
			}
		}(addr)
	}

	// tokens
	for _, token := range c.Config.Tokens {
		go func(token string) {
			m := metric.New("balance", c.VictoriaMetrics, map[string]string{"version": version, "token": token}, metricLogger)
			for {
				select {
				default:
					m.FromFunction(func() (float64, error) {
						f, err := c.GetTokenSupply(token)
						if err != nil {
							return -1, fmt.Errorf("failed to get supply for %s: %s", token, err)
						}
						return f, nil
					})
					time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
				case <-ctx.Done():
					break
				}
			}
		}(token)
	}

	// minimum_ledger_slot
	go func() {
		m := metric.New("minimum_ledger_slot", c.VictoriaMetrics, map[string]string{"version": version}, metricLogger)
		for {
			select {
			default:
				m.FromFunction(func() (float64, error) {
					f, err := c.MinimumLedgerSlot()
					if err != nil {
						return -1, fmt.Errorf("failed to get minimum_ledger_slot: %s", err)
					}
					return f, nil
				})
				time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
			case <-ctx.Done():
				break
			}
		}
	}()
}
