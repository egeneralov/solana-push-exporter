package rpc

type Config struct {
	Endpoint      string   `json:"endpoint" yaml:"endpoint"`
	Addresses     []string `json:"addresses" yaml:"addresses"`
	Tokens        []string `json:"tokens" yaml:"tokens"`
	Sleep         int      `json:"sleep" yaml:"sleep"`
	Identity      string   `json:"identity" yaml:"identity"`
	VotePublicKey string   `json:"votepubkey" yaml:"votepubkey"`
}
