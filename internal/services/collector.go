package services

import (
	"context"
	"fmt"

	"gitlab.com/egeneralov/solana-push-exporter/internal/helpers/ipinfo"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/binance"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/kyc"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/rpc"
	"gitlab.com/egeneralov/solana-push-exporter/internal/types/config"
	"gitlab.com/egeneralov/solana-push-exporter/internal/types/metric"
	"go.uber.org/zap"
)

func Collector(ctx context.Context, cfg config.Config, logger *zap.Logger) error {
	// auto-detect node identity
	if identityInfo, err := rpc.New(cfg.RPC, cfg.VictoriaMetrics, logger.With(zap.String("system", "rpc"))).GetIdentity(); err != nil {
		return err
	} else {
		identity := identityInfo.Result.Identity
		if identity == "" {
			return fmt.Errorf("recived identity are nil")
		}
		cfg.RPC.Addresses = append(cfg.RPC.Addresses, identity)
		if cfg.RPC.Identity == "" {
			cfg.RPC.Identity = identity
		}
		if cfg.RPC.VotePublicKey == "" {
			cfg.RPC.VotePublicKey = identity
		}
		if cfg.KYC.Identity == "" {
			cfg.KYC.Identity = identity
		}
		metric.DefaultLabels["identity"] = identity
	}

	// auto-detect node ip address
	if ip, err := ipinfo.IpInfo(); err != nil {
		logger.Warn("failed to gather ip address", zap.Error(err))
	} else {
		metric.DefaultLabels["ip"] = ip
	}

	go rpc.New(cfg.RPC, cfg.VictoriaMetrics, logger.With(zap.String("system", "rpc"))).Collector(ctx)
	go binance.New(cfg.Binance, cfg.VictoriaMetrics, logger.With(zap.String("system", "binance"))).Collector(ctx)
	go kyc.New(cfg.KYC, logger.With(zap.String("system", "kyc"))).Collector(ctx)

	return nil
}
