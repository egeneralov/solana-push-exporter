package kyc

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/egeneralov/solana-push-exporter/internal/services/hclient"
	"gitlab.com/egeneralov/solana-push-exporter/internal/types/metric"
	"go.uber.org/zap"
)

func New(cfg Config, logger *zap.Logger) KYC {
	logger.Sugar().Info("created kyc.Client", cfg)
	return KYC{
		HTTPClient: &hclient.HClient,
		Config:     cfg,
		Logger:     logger,
	}
}

type KYC struct {
	HTTPClient      *http.Client
	Config          Config
	VictoriaMetrics string
	Logger          *zap.Logger
}

type ValidatorStats struct {
	AvgSkipRate               float64 `json:"avg_skip_rate"`
	AvgSelfStake              float64 `json:"avg_self_stake"`
	NumBonusLast10            float64 `json:"num_bonus_last_10"`
	VoteCreditScore           float64 `json:"vote_credit_score"`
	BonusEpochsPercent        float64 `json:"bonus_epochs_percent"`
	BonusAndBaselinePercent   float64 `json:"bonus_and_baseline_percent"`
	VoteCreditsLast64Epochs   float64 `json:"vote_credits_last_64_epochs"`
	PercentBonusLast13Epochs  float64 `json:"percent_bonus_last_13_epochs"`
	PercentBonusSinceAug12021 float64 `json:"percent_bonus_since_aug1_2021"`
}

type ValidatorInfo struct {
	TestnetPk          string         `json:"testnet_pk"`
	MainnetBetaPk      string         `json:"mainnet_beta_pk"`
	State              string         `json:"state"`
	MnName             string         `json:"mn_name"`
	MnKeybaseUsername  string         `json:"mn_keybase_username"`
	TnName             string         `json:"tn_name"`
	OnboardingNumber   float64        `json:"onboarding_number"`
	TdsOnboardingGroup float64        `json:"tds_onboarding_group"`
	MnCalculatedStats  ValidatorStats `json:"mn_calculated_stats"`
	TnCalculatedStats  ValidatorStats `json:"tn_calculated_stats"`
	TnStats            struct {
		Epochs map[string]string `json:"epochs"`
	} `json:"tn_stats"`
	MnStats struct {
		Epochs map[string]string `json:"epochs"`
	} `json:"mn_stats"`
}

type ValidatorsAnswer struct {
	Offset  float64         `json:"offset"`
	Limit   float64         `json:"limit"`
	Data    []ValidatorInfo `json:"data"`
	OrderBy string          `json:"order_by"`
	Order   string          `json:"order"`
}

func (k KYC) Info() (*ValidatorsAnswer, error) {
	rawURL := "https://kyc-api.vercel.app/api/validators/list?limit=100&search_term=" + k.Config.Identity
	response, err := k.HTTPClient.Get(rawURL)
	if err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("invalid status code for %s: %d", rawURL, response.StatusCode)
	}
	var (
		decoder = json.NewDecoder(response.Body)
		a       ValidatorsAnswer
	)
	if err := decoder.Decode(&a); err != nil {
		return nil, err
	}
	return &a, nil
}

func (k KYC) Collector(ctx context.Context) {
	l := k.Logger.With(zap.String("subsystem", "metric"))
	for {
		select {
		default:
			va, err := k.Info()
			if err == nil {
				for _, validator := range va.Data {
					labels := map[string]string{
						"testnet_pk":          validator.TestnetPk,
						"mainnet_beta_pk":     validator.MainnetBetaPk,
						"mn_name":             validator.MnName,
						"tn_name":             validator.TnName,
						"mn_keybase_username": validator.MnKeybaseUsername,
					}

					// shared
					if err := metric.New("onboarding_number", k.VictoriaMetrics, labels, l).StoreAndSend(validator.OnboardingNumber); err != nil {
						l.Warn("failed to send metric onboard_number", zap.Error(err))
					}
					if err := metric.New("tds_onboarding_group", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TdsOnboardingGroup); err != nil {
						l.Warn("failed to send metric tds_onboarding_group", zap.Error(err))
					}

					// mn_
					if err := metric.New("mn_calculated_stats_avg_skip_rate", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.AvgSkipRate); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_avg_skip_rate", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_avg_self_stake", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.AvgSelfStake); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_avg_self_stake", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_num_bonus_last_10", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.NumBonusLast10); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_num_bonus_last_10", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_vote_credit_score", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.VoteCreditScore); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_vote_credit_score", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_bonus_epochs_percent", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.BonusEpochsPercent); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_bonus_epochs_percent", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_bonus_and_baseline_percent", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.BonusAndBaselinePercent); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_bonus_and_baseline_percent", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_vote_credits_last_64_epochs", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.VoteCreditsLast64Epochs); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_vote_credits_last_64_epochs", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_percent_bonus_last_13_epochs", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.PercentBonusLast13Epochs); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_percent_bonus_last_13_epochs", zap.Error(err))
					}
					if err := metric.New("mn_calculated_stats_percent_bonus_since_aug1_2021", k.VictoriaMetrics, labels, l).StoreAndSend(validator.MnCalculatedStats.PercentBonusSinceAug12021); err != nil {
						l.Warn("failed to send metric mn_calculated_stats_percent_bonus_since_aug1_2021", zap.Error(err))
					}

					// tn_
					if err := metric.New("tn_calculated_stats_avg_skip_rate", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.AvgSkipRate); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_avg_skip_rate", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_avg_self_stake", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.AvgSelfStake); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_avg_self_stake", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_num_bonus_last_10", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.NumBonusLast10); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_num_bonus_last_10", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_vote_credit_score", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.VoteCreditScore); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_vote_credit_score", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_bonus_epochs_percent", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.BonusEpochsPercent); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_bonus_epochs_percent", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_bonus_and_baseline_percent", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.BonusAndBaselinePercent); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_bonus_and_baseline_percent", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_vote_credits_last_64_epochs", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.VoteCreditsLast64Epochs); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_vote_credits_last_64_epochs", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_percent_bonus_last_13_epochs", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.PercentBonusLast13Epochs); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_percent_bonus_last_13_epochs", zap.Error(err))
					}
					if err := metric.New("tn_calculated_stats_percent_bonus_since_aug1_2021", k.VictoriaMetrics, labels, l).StoreAndSend(validator.TnCalculatedStats.PercentBonusSinceAug12021); err != nil {
						l.Warn("failed to send metric tn_calculated_stats_percent_bonus_since_aug1_2021", zap.Error(err))
					}

				}
			} else {
				k.Logger.Warn("KYC.Info()", zap.Error(err))
				continue
			}
			time.Sleep(time.Duration(k.Config.Sleep) * time.Second)
		case <-ctx.Done():
			break
		}
	}
}
