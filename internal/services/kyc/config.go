package kyc

type Config struct {
	Sleep    int    `json:"sleep" yaml:"sleep"`
	Identity string `json:"identity" yaml:"identity"`
}
