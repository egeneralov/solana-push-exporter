package binance

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitlab.com/egeneralov/solana-push-exporter/internal/services/hclient"
	"gitlab.com/egeneralov/solana-push-exporter/internal/types/metric"
	"go.uber.org/zap"
)

type answer struct {
	Symbol string `json:"symbol"`
	Price  string `json:"price"`
}

func New(cfg Config, vm string, logger *zap.Logger) Client {
	logger.Sugar().Info("created binance.Client", cfg)
	return Client{
		HTTPClient:      &hclient.HClient,
		Config:          cfg,
		VictoriaMetrics: vm,
		Logger:          logger,
	}
}

type Client struct {
	HTTPClient      *http.Client
	Config          Config
	VictoriaMetrics string
	Logger          *zap.Logger
}

func (c Client) Price(ticker string) (float64, error) {
	rawURL := "https://api.binance.com/api/v3/ticker/price?symbol=" + ticker
	response, err := c.HTTPClient.Get(rawURL)
	if err != nil {
		return -1, err
	}
	if response.StatusCode != http.StatusOK {
		return -1, fmt.Errorf("invalid status code for %s: %d", rawURL, response.StatusCode)
	}
	var (
		decoder = json.NewDecoder(response.Body)
		a       answer
	)
	if err := decoder.Decode(&a); err != nil {
		return -1, err
	}
	return strconv.ParseFloat(a.Price, 64)
}

func (c Client) Collector(ctx context.Context) {
	wg := sync.WaitGroup{}
	for _, ticker := range c.Config.Symbols {
		wg.Add(1)
		go func(ticker string) {
			defer wg.Done()
			m := metric.New("binance_price", c.VictoriaMetrics, map[string]string{"ticker": ticker}, c.Logger.With(zap.String("subsystem", "metric")))
			for {
				select {
				default:
					m.FromFunction(func() (float64, error) {
						f, err := c.Price(ticker)
						if err != nil {
							return -1, fmt.Errorf("failed to get price: %s", err)
						}
						return f, nil
					})
					time.Sleep(time.Duration(c.Config.Sleep) * time.Second)
				case <-ctx.Done():
					break
				}
			}
		}(ticker)
	}
	wg.Wait()
}
