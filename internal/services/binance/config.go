package binance

type Config struct {
	Sleep   int      `json:"sleep" yaml:"sleep"`
	Symbols []string `json:"symbols" yaml:"symbols"`
}
