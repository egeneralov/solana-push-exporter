FROM golang:1.17.3
RUN apt-get update -q && apt-get install -yq ca-certificates
ENV \
  CGO_ENABLED=0 \
  GOOS=linux \
  GOARCH=amd64
WORKDIR /go/src/gitlab.com/egeneralov/solana-push-exporter/cmd/solana-push-exporter
ADD go.mod go.sum /go/src/gitlab.com/egeneralov/solana-push-exporter/cmd/solana-push-exporter/
RUN go mod download -x
ADD . .
RUN go build -v -installsuffix cgo -ldflags="-w -s" -o /go/bin/solana-push-exporter gitlab.com/egeneralov/solana-push-exporter/cmd/solana-push-exporter

FROM debian:buster
RUN apt-get update -q && apt-get install -yq ca-certificates
ENV PATH='/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
CMD /go/bin/solana-push-exporter
COPY --from=0 /go/bin /go/bin