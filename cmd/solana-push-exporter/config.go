package main

import (
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/binance"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/kyc"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services/rpc"
	"gitlab.com/egeneralov/solana-push-exporter/internal/types/config"
)

const (
	defaultSleep = 30
)

var (
	cfg = config.Config{
		RPC: rpc.Config{
			Sleep: defaultSleep,
		},
		Binance: binance.Config{
			Sleep: defaultSleep,
			Symbols: []string{
				"SOLUSDT",
			},
		},
		KYC: kyc.Config{
			Sleep: defaultSleep * 4,
		},
	}
)
