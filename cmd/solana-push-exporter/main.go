package main

import (
	"context"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/cristalhq/aconfig"
	"github.com/cristalhq/aconfig/aconfigyaml"
	"gitlab.com/egeneralov/solana-push-exporter/internal/services"
	"go.uber.org/zap"
)

const (
	ServiceName = "solana_push_exporter"
)

var (
	logger, _  = zap.NewProduction()
	yamlLoader = aconfigyaml.New()

	signals = make(chan os.Signal, 1)
)

func init() {
	if err := aconfig.LoaderFor(&cfg, aconfig.Config{
		EnvPrefix:  strings.ToUpper(ServiceName),
		FlagPrefix: ServiceName,
		Files: []string{
			filepath.Join("/etc/", ServiceName+".yaml"),
			filepath.Join("config/", ServiceName+".yaml"),
			filepath.Join(".", ServiceName+".yaml"),
			filepath.Join("/etc/", ServiceName+".yml"),
			filepath.Join("config/", ServiceName+".yml"),
			filepath.Join(".", ServiceName+".yml"),
		},
		FileDecoders: map[string]aconfig.FileDecoder{
			".yaml": yamlLoader,
			".yml":  yamlLoader,
		},
	}).Load(); err != nil {
		logger.Fatal("failed to load configuration", zap.Error(err))
	}

	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	if err := services.Collector(ctx, cfg, logger); err != nil {
		logger.Error("failed to start collectors", zap.Error(err))
	}
	sig := <-signals
	cancel()
	logger.Info("cancel", zap.String("signal", sig.String()))
}
