module gitlab.com/egeneralov/solana-push-exporter

go 1.17

require (
	github.com/cristalhq/aconfig v0.16.8
	github.com/cristalhq/aconfig/aconfigyaml v0.16.1
	github.com/portto/solana-go-sdk v1.12.0
	go.uber.org/atomic v1.9.0
	go.uber.org/zap v1.19.1
)

require (
	filippo.io/edwards25519 v1.0.0-rc.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mr-tron/base58 v1.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
